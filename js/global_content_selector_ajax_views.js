(function ($, Drupal) {
    var globalContentSelectorLoadViews;
    Drupal.behaviors.globalContentSelectorLoadViews = {
        attach: function (context, settings) {
            globalContentSelectorLoadViews = this;
            $('.view.ajax-load').each(function () {
                globalContentSelectorLoadViews.ajaxLoad($(this), context, settings);
            });
        },
        ajaxLoad: function (view, context, settings) {
            var params = '';
            var url = document.location.protocol + '//';
            var viewUuid = view.attr('id');
            var viewName = settings.globalContentSelector.viewsData[viewUuid].viewName;
            var viewDisplayName = settings.globalContentSelector.viewsData[viewUuid].viewDisplayName;
            var cookieVal = Drupal.behaviors.globalContentSelector.getCookie(context, settings);
            var customParams = globalContentSelectorLoadViews.ajaxUrlAddParams(context, settings);
            $('.view-' + viewName + '.display-' + viewDisplayName).html(settings.globalContentSelector.loadingMarkup);
            params = 'view_name=' + viewName;
            params += '*' + 'view_display=' + viewDisplayName;
            params += '*' + 'cookie_val=' + cookieVal;
            url += document.location.host;
            url += "/global_content_selector/ajax/";
            if (customParams.length) {
                if (customParams.substr(0, 1) != '/') {
                    customParams = '/' + customParams;
                }
                params += customParams;
            }
            //@TODO
            //Change this manualy made AJAX to be made by Drupal.ajax
            //
            $.ajax({
                url: url + params,
                cache: true
            }).done(function (html) {
                $('#'+viewUuid).html(html);
                Drupal.behaviors.globalContentSelector.setSelectFromCookie(context, settings);
                globalContentSelectorLoadViews.runAjaxCallback(html, context, settings);
            });
        },
        ajaxCallback: {},
        runAjaxCallback: function (html, context, settings) {
            context = context || document;
            settings = settings || Drupal.settings;
            // Execute all of them.
            $.each(this.ajaxCallback, function () {
                if ($.isFunction(this.attach)) {
                    this.attach(html, context, settings);
                }
            });
        },
        ajaxUrlParams: {},
        ajaxUrlAddParams: function (context, settings) {
            context = context || document;
            settings = settings || Drupal.settings;
            // Execute all of them.
            var returnValues = '';
            $.each(this.ajaxUrlParams, function () {
                if ($.isFunction(this.attach)) {
                    returnValues += '*' + this.attach(context, settings);
                }
            });
            return returnValues;
        },
    };
}(jQuery, Drupal));