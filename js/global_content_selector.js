(function ($, Drupal) {
    var globalContentSelector;
    var form;
    var formSelect;
    Drupal.behaviors.globalContentSelector = {
        attach: function (context, settings) {
            form = 'form[id|=\'global-content-selector-select-form\']';
            formSelect = 'select.form-select';
            globalContentSelector = this;
            this.setSelectFromCookie(context, settings);

            $('body').delegate(formSelect, 'change', function (e) {
                if ($(this).parents('form').find('input.form-submit').length <= 0) {
                    e.preventDefault();
                    var selectedVal = $(this).val();
                    globalContentSelector.requestContent(selectedVal, context, settings);
                }
            });
            $('body').delegate(form, 'submit', function (e) {
                e.preventDefault();
                var selectedVal = $(this).find('select.form-select').val();
                globalContentSelector.requestContent(selectedVal, context, settings);
            });
        },
        requestContent: function (selectedVal, context, settings) {
            this.setCookie(selectedVal, context, settings);
            Drupal.behaviors.globalContentSelectorLoadViews.attach(context, settings);
        },
        setSelectFromCookie: function (context, settings) {
            var cookieVal = this.getCookie(context, settings);
            if ($(formSelect + ' option').length) {
                $(formSelect + ' option').removeAttr('selected').filter('[value=' + cookieVal + ']').attr('selected', true);
            }
        },
        getCookie: function (context, settings) {
            var cookieVal = $.cookie('globalContentSelectorSelection');
            return this.getValidCookieVal(cookieVal, context, settings);
        },
        setCookie: function (cookieVal, context, settings) {
            cookieVal = this.getValidCookieVal(cookieVal, context, settings);
            $.cookie('globalContentSelectorSelection', cookieVal, { path: '/' });
        },
        getValidCookieVal: function (cookieVal, context, settings) {
            if (cookieVal === "undefined" || cookieVal === null) {
                cookieVal = Drupal.settings.globalContentSelector.defaultSelection;
                this.setCookie(cookieVal, context, settings);
            }
            return cookieVal;
        }
    };
}(jQuery, Drupal));