<?php

/**
 * @file
 * global_content_selector_views.inc
 *
 * Defines the Global Content Selector default argument plugins.
 */

/**
 * Implements hook_views_plugins().
 *
 * Note that while contextual filters are assigned to fields, contextual filter
 * 'argument default' plugins are global and as far as I can see, unconditional.
 */
function global_content_selector_views_plugins(){
    $plugins = array(
      'argument default' => array(
        'global_content_selector_field' => array(
          'title' => t('Global content selector (field or search term)'),
          'handler' => 'global_content_selector_plugin_argument_default_global_content_selector_field',
        ),
      ),
    );
    return $plugins;
}
