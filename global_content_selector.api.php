<?php

/**
 * @file
 * Describe hooks provided by the Global content selector module.
 */
/**
 * @mainpage Global content selector API Manual
 * *
 * Topics:
 * - @link global_content_selector_views_theme_alter Setting a different theme
 *  for rendering the view.@endlink
 * - @link global_content_selector_hooks Global content selector hooks @endlink
 */
/**
 * @defgroup global_content_selector_how_it_works How Global content selector
 * works
 * @{
 * This page explains in few words how the module works and how to use it.
 *
 * Since this module try to keep as simple as possible, for the Global content
 * selector be able to recognize a view, is necessary to implement a few hooks
 * to set it up.
 *
 * For the module to re
 *
 * @}
 */
/**
 * @defgroup global_content_selector_hooks Global content selector hooks
 * @{
 * Hooks that can be implemented by other modules in order to set up and use the
 * Global content selector module.
 */

/**
 * Provide to Global content selector the views to be recognized by the module.
 *
 * @param $global_selected_views
 *   An array of all Views and display names.
 */
function hook_global_content_selector_views_include(){
    $global_selected_views = array(
      'webdoor_home' => 'panel_pane_1',
      'blocos_segmentados' => 'panel_pane_1',
    );
    return $global_selected_views;
}

/**
 * Set the theme to be used to render the view.
 *
 * @return $theme_key
 *   The theme_key to be used when rendering the views wich uses Global content
 *   selector
 */
function hook_global_content_selector_views_theme_alter($view_name, $view_display){
    $theme_key = 'ember';
    switch($view_name){
        case 'foo':
            $theme_key = 'bartik';
            break;
        case 'my_view':
            $theme_key = 'radix';
            break;
    }
    return $theme_key;
}

/**
 * @}
 */
